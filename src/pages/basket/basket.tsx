import React, { useEffect, useState } from "react";
import { EuiCard, EuiIcon, EuiBadge, EuiButton } from "@elastic/eui";
import "./basket.scss";
import { useLocalStorage } from "../../hooks/useLocalStorage";
import { deleteAllCookies } from "../../utils/index";
import { getCart } from "../../service/apiService";
import useSWR from "swr";
import { apiService } from "../../service/axiosConfig";

const Basket = () => {
	const [localUser] = useLocalStorage("user", "");
	const fetcher = (url: any) => apiService.get(url).then((res) => res.data);

	const { data: products, isLoading } = useSWR("/cart", fetcher);

	const handleLogout = () => {
		deleteAllCookies();
		localStorage.clear();
		window.location.assign("/login");
	};

	return (
		<div className="basket">
			<div className="header">
				<span className="title"> Basket</span>
				<div className="right">
					<EuiBadge color="danger">{localUser.email}</EuiBadge>
					<EuiButton onClick={handleLogout}>Log out</EuiButton>
				</div>
			</div>
			<div className="products">
				{isLoading ? (
					<span>loading</span>
				) : (
					products.map((product: any) => (
						<EuiCard
							textAlign="left"
							href="https://elastic.github.io/eui/"
							image={product.image}
							icon={<EuiIcon size="xxl" type="logoBeats" />}
							title={product.name}
							description={`Price: ${product.price}`}
						/>
					))
				)}
			</div>
		</div>
	);
};

export default Basket;
