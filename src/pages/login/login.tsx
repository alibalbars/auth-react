import React, { useState } from "react";
import {
	EuiButton,
	EuiText,
	EuiFieldText,
	EuiPanel,
	EuiFieldPassword,
	EuiToast,
} from "@elastic/eui";
import "@elastic/eui/dist/eui_theme_light.css";
import "./login.scss";
import { signUp, login } from "../../service/apiService";
import { useAppError } from "../../context/AppErrorContext";
import { useNavigate } from "react-router-dom";
import { useLocalStorage } from "../../hooks/useLocalStorage";

type toastColorType = "success" | "primary" | "warning" | "danger" | undefined;

interface IToast {
	open: boolean;
	title: string;
	color: toastColorType;
	body: string;
}

const Login = () => {
	const { error } = useAppError();
	const navigate = useNavigate();
	const [localUser, setLocalUser] = useLocalStorage("user", "");

	console.log("message:", error.message);
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [toast, setToast] = useState<IToast>({
		open: false,
		title: "deneme 1",
		color: "primary",
		body: "",
	});

	const handleSignUp = async () => {
		const signUpData = await signUp({ email, password });
		console.log("signUpData:", signUpData);
		if (signUpData) {
			setToast({
				open: true,
				title: "",
				color: "success",
				body: "Successfully signed up.",
			});
		} else {
			setToast({
				open: true,
				title: "",
				color: "danger",
				body: "User already exists!",
			});
		}
	};

	const handleLogin = async () => {
		const loginData = await login({ email, password });
		console.log("loginData:", loginData);
		if (loginData) {
			setLocalUser({ email: loginData.data.email });
			navigate("/basket");
		} else {
			setToast({
				open: true,
				title: "",
				color: "danger",
				body: "Email or password is wrong!",
			});
		}
	};

	const onEmailChange = (e: any) => {
		setEmail(e.target.value);
	};

	const onPasswordChange = (e: any) => {
		setPassword(e.target.value);
	};

	const handleToastClose = () => {
		setToast((toast) => ({ ...toast, open: false }));
	};

	return (
		<div className="login-container">
			<EuiPanel className="custom-panel">
				<EuiText>Login / Signup</EuiText>
				<div className="input-container">
					<EuiFieldText
						placeholder="Email"
						className="custom-input"
						value={email}
						onChange={onEmailChange}
					/>
					<EuiFieldPassword
						placeholder="Password"
						type="dual"
						className="custom-input"
						value={password}
						onChange={onPasswordChange}
					/>
				</div>
				<div className="button-container">
					<EuiButton fill onClick={handleSignUp}>
						Sign up
					</EuiButton>
					<EuiButton onClick={handleLogin}>Login</EuiButton>
				</div>
			</EuiPanel>
			{toast.open ? (
				<EuiToast
					title={toast.title}
					onClose={handleToastClose}
					color={toast.color}
				>
					{toast.body}
				</EuiToast>
			) : (
				<></>
			)}
		</div>
	);
};

export default Login;
