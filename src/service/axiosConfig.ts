import axios from "axios";

const apiService = axios.create({
	baseURL: process.env.API_URL,
	timeout: 1000,
	withCredentials: true,
	headers: {
		"Content-type": "application/json",
	},
});

console.log("test");

apiService.interceptors.response.use(
	(response) => {
		// This function will be called for successful responses
		return response;
	},
	(error) => {
		// This function will be called for error responses
		// You can use this function to run a callback or perform other operations when an error occurs
		// The error object contains information about the error, including the response data if available
		// console.error(error);
		// return Promise.reject(error);x
	}
);

export { apiService };
