import { apiService } from "./axiosConfig";

interface ISignUp {
	email: string;
	password: string;
}
interface ILogin extends ISignUp {}

export const signUp = async (props: ISignUp) => {
	const { email, password } = props;
	return apiService.post("/signup", { email, password });
};

export const login = async (props: ILogin) => {
	const { email, password } = props;
	return apiService.post("/login", { email, password });
};

export const getCart = async () => {
	return apiService.get("/cart");
};
