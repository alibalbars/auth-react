import { useNavigate } from "react-router-dom";
import { useLocalStorage } from "../hooks/useLocalStorage";

export const ProtectedRoute = ({ children }: any) => {
	const [localUser, setLocalUser] = useLocalStorage("user", "");
	const navigate = useNavigate();
	if (!localUser) {
		// user is not authenticated
		window.location.assign("/login");
		return;
	}
	return children;
};
