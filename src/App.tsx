import React from "react";
import "./App.scss";
import "./index.scss";
import { router } from "./routes";
import { RouterProvider } from "react-router-dom";
import { AppErrorContext } from "./context/AppErrorContext";

const App = () => {
	return (
		<div className="app">
			<AppErrorContext.Provider value={{ message: "appten" }}>
				{/* <RouterProvider router={router} /> */}
				{router}
			</AppErrorContext.Provider>
		</div>
	);
};

export default App;
