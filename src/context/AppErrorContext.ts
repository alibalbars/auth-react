import { createContext, useContext, useState } from "react";

interface IAppErrorContext {
	statusCode?: number;
	message: string;
}

const AppErrorContext = createContext({ message: "" });

const useAppError = () => {
	const { statusCode, message } = useContext<IAppErrorContext>(AppErrorContext);
	const [error, setError] = useState({ statusCode, message });

	return { error, setError };
};

export { useAppError, AppErrorContext };
