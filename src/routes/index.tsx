import React from "react";
import {
	createBrowserRouter,
	Route,
	Routes,
	createRoutesFromElements,
	BrowserRouter as Router,
} from "react-router-dom";
import Login from "../pages/login/login";
import Basket from "../pages/basket/basket";
import { ProtectedRoute } from "../components/ProtectedRoute";

export const router = (
	<Router>
		<Routes>
			<Route path="/login" element={<Login />} />
			<Route
				path="/basket"
				element={
					<ProtectedRoute>
						<Basket />
					</ProtectedRoute>
				}
			/>
			<Route path="/" element={<span>Home</span>} />
		</Routes>
	</Router>
);

// export const router = createBrowserRouter(
// 	createRoutesFromElements(
// 		<Routes>
// 			<Route path="/login" element={<Login />} />
// 			<ProtectedRoute>
// 				<Route path="/basket" element={<Basket />} />
// 			</ProtectedRoute>
// 		</Routes>
// 	)
// );

// export const router = createBrowserRouter([
// 	{
// 		path: "/",
// 		element: <div>Hello world!111</div>,
// 	},
// 	{
// 		path: "/login",
// 		element: <Login />,
// 	},
// 	{
// 		path: "/basket",
// 		element: <Basket />,
// 	},
// 	{
// 		path: "*",
// 		element: <span>404</span>,
// 	},
// ]);
