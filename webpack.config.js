const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
require("dotenv").config({ path: "./.env" });

const env = "development";

module.exports = {
	mode: env,
	entry: "./src/index.tsx",
	module: {
		rules: [
			{
				test: /\.m?[jt]sx?$/,
				exclude: /(node_modules)/,
				use: {
					// `.swcrc` can be used to configure swc
					loader: "swc-loader",
					// to transform index.ts file
					options: {
						jsc: {
							parser: {
								syntax: "typescript",
							},
						},
					},
				},
			},
			{
				test: /\.s?[ac]ss$/i,
				use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
			},
		],
	},
	devServer: {
		static: {
			directory: path.join(__dirname, "dist"),
		},
		compress: true,
		port: 9000,
		hot: true,
		historyApiFallback: true,
		// publicPath: "/",
	},
	plugins: [
		new HtmlWebpackPlugin({
			title: "my react app",
			template: "./public/index.html",
		}),
		new webpack.DefinePlugin({
			"process.env": JSON.stringify(process.env),
			// "process.env": {
			// 	NODE_ENV: JSON.stringify(env),
			// },
		}),
		new MiniCssExtractPlugin(),
	],
	resolve: {
		extensions: [".js", ".jsx", ".ts", ".tsx"],
	},
};
